# Un par de cosas que debes saber

## Autenticación



Todos los endpoint de la API necesitan autenticación para poder ejecutarlos, para ello necesita primero ingresar un usuario válido en el *endpoint Login del módulo Auth*, este módulo verificará que el usuario que ingrese este activo, le regresará un JSON con la siguiente información:

```js
{
    "usuario_id: 12345,
    "api_key": "apiKeyCode",
    "cliente_id": 985,
    "...":"..."
}
```

Estas credenciales serán válidas solo por un par de horas, pasado ese tiempo tendrá que generar un nuevo login.

En cada petición tendrá que incluir la API KEY dentro de la autenticación como **bearer token**:
```js
var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
myHeaders.append("Authorization", "Bearer SomeApiKeyCode1234567890==");

var requestOptions =  {
  headers: myHeaders
}
```

## Envío de archivos

 Para que los endpoints que reciban archivos puedan aceptar la petición y no devolver error, el archivo tiene que mandarse en **un body como form data**, además, en el body tiene que llevar el nombre "*data*" como se muestra en este ejemplo de una petición válida realizada en Postman.

![Ejemplo de una petición con formd-data](./img/form_data.png)

Puede guiarse también de los siguientes ejemplos de javascript y python autogenerados por Postman:

### Javascript

```js
var myHeaders = new Headers();
myHeaders.append("api-key", "apiKeyCode");
myHeaders.append("usuario-id", "12345");

var formdata = new FormData();
formdata.append("data", fileInput.files[0], "formato (21).xlsx");

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: formdata,
  redirect: 'follow'
};

fetch("http://0.0.0.0:8000/dashboard/upload/?formato=1", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

### Python

```py
import requests

url = "http://0.0.0.0:8000/dashboard/upload/?formato=1"

payload = {}
files=[
  ('data',('formato (21).xlsx',open('/home/eddy/Downloads/formato (21).xlsx','rb'),'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'))
]
headers = {
  'api-key': 'apiKeyCode',
  'usuario-id': '12345'
}

response = requests.request("POST", url, headers=headers, data=payload, files=files)

print(response.text)

```

## Agregando nuevos orígenes 

Si se necesita hacer peticiones a la API desde un origen específico, puede ir al archivo **main.py** y agregarlo dentro de la lista de la variable **origins**, al hacer eso evitará errores por el CORS de la aplicación

![Variable con los orígenes  permitidos](./img/origins.png)

## Cambiando entre pruebas y producción

Dentro del archivo *sql/connection.py* encontrará las siguientes funciones:

+ db_connection_data: Apunta hacía la base de datos de pruebas de PogenData
+ db_connection_retail: Apunta hacía la base de datos de prubeas de Retail
+ prod_connection_data: Apunta hacía la base de datos de **producción de PogenData**
+ prod_connection_retail: Apunta hacía la base de datos de **producción de Retail**

Observe con atención la siguiente imagen:

![Ejemplo de conexión](./img/conexiones.png)

En el punto 1 se indica la importación de la conexión de la base de datos de prueba de PogenData, el punto 2 señala una variable llamada "con" (usualmente tiene este nombre, pero puede variar en algunos endpoints), lo que está pasando es que a esa variable se le está pasando la conexión de la base de datos que importamos. 
Repitiendo este proceso, si se quisiese apuntar a la base de datos de producción se tendrá que cambiar por la función "*prod_conection_data*" despues de ser importada al módulo:

![Ejemplo de cambio conexión](./img/cambio_conexion.png)

```sh
Nota: tenga cuidado el especificar los nombres y valores de las variables de entorno de las bases de datos en el .env, una equivocación y APUNTARÍA A PRODUCCIÓN EN LUGAR DE PRUEBAS
```