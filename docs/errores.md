#Problemas comunes

Dentro de esta sección puede consultar errores que pueden ser comunes a la hora de intentar levantar la API o al momento de mandar a llamar a un endpoint (si ve algún error habitual puede reportarlo para que pueda ser añadido a esta sección).

## Cannot access variable 'con' (error de credenciales)

Si un endpoint marca un error y el output de la API en el shell es el siguiente

![Error de conexión](./img/error_con.png)

Significa que no se pudo efectuar la conexión a la base de datos, en el caso de la imagen se tiene un error de conexión con la base de datos de prueba de Retail, para solucionar esto verifique si las credenciales que se están utilizando son validas, y si lo son, verifique que esten declaradas y escritas correctamente dentro del archivo .env