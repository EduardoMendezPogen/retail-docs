# Antes de iniciar

## Primeros pasos

Antes de ejecutar la API, se recomienda crear un entorno virtual para instalar todas las dependencias dentro de este, para hacerlo, ejecute en el intérprete de comandos la instrucción para crear un entorno virtual con Python:

```sh
python -m venv /path/to/venv
```

Para activarlo, posicionese en la carpeta donde fue creado el entorno virtual y activelo con el comando:

```sh
source bin/activate
```
Una vez activado correctamente el entorno, proceda a installar las dependencias del archivo *requirements.txt* que se encuentra en la carpeta raíz del proyecto ejecutando el siguiente comando:

```sh
pip install -r requirements.txt
```

**NOTA: la aplicación esta creada con Python 11, por lo que es posible que algunas versiones de las librerías no esten disponibles para otras versiones de Python por lo que tendrán que ser instaladas manualmente, es posible que el proyecto funcione de manera extraña con otras versiones de las librerías.**

## Variables de entorno

Por seguridad, los datos sensibles dentro de la aplicación no estan escritos en texto plano, estos datos se consiguen a través de un archivo *.env* que usted deberá crear en **la carpeta raíz** del proyecto al momento de descargar el repositorio, esta es la lista de las variables de entorno necesarias para que la aplicación funcione correctamente

+ RETAIL_HOST : Host de la base de datos de Retail
+ RETAIL_DB : Base de datos de Retail a la que desea conectarse
+ RETAIL_USER : Usuario de la base de datos de Retail
+ RETAIL_PW : Contraseña del usuario Retail
+ RETAIL_PORT (opcional) : Puerto por el cual se conecta a Retail 
+ DATA_HOST : Host de la base de datos de PogenData
+ DATA_DB : Base de datos de PogenData a la que desea conectarse
+ DATA_USER : Usuario de la base de datos de PogenData
+ DATA_PW : Contraseña del usuario PogenData
+ DATA_PORT (opcional) : Puerto por el cual se conecta a PogenData
+ PROD_DATA_HOST: Host de la base de datos de **producción** de PogenData
+ PROD_DATA_DB: Base de de datos de **producción** de PogenData a la cual desea conectarse
+ PROD_DATA_USER: Usuario de la base de datos de **producción** de PogenData
+ PROD_DATA_PW: Contraseña del usuaro de PogenData de **producción**
+ PROD_RETAIL_PORT (opcional) : Puerto por el cual se conecta a **producción** de Retail
+ PROD_RETAIL_HOST: Host de la base de datos de **producción** de Retail
+ PROD_RETAIL_DB: Base de de datos de **producción** de Retail a la cual desea conectarse
+ PROD_RETAIL_USER: Usuario de la base de datos de **producción** de Retail
+ PROD_RETAIL_PW: Contraseña del usuaro de Retail de **producción**
+ PROD_RETAIL_PORT (opcional) : Puerto por el cual se conecta a **producción** de Retail
+ SANDBOX_EMAIL : Email utilizado para identificarse al enviar los correos de mailgun
+ MAILGUN_APIKEY : API Key para utilizar la API de Mailgun
+ MAILGUN_ENDPOINT: Endpoint para utilizar la API de Mailgun
+ POGEN_DATA_API: Url base de la API de PogenData

## Iniciando el proyecto

Habiendo instalado las dependencias necesarias para el proyecto, ejecute el siguiente comando para iniciar un servidor de Uvicorn desde la carpeta del proyecto

```sh
uvicorn main:app --host 0.0.0.0 --port 8000
```

El comando de uvicorn recibe como parámetro el nombre del archivo .py principal del proyecto, junto con ello, también se recibe el nombre del objeto FastAPI que contiene toda la aplicación, además recibe como opciones el host y el puerto en el que se desea ejecutar (*también es posible agregar la opción **--reload** por si es necesario editar el código y que se actualize la API en automático cada vez que el código es modificado*).
Al ejecutar este comando verá el siguiente output en la terminal:

![Aplicación iniciada en consola](./img/output.png)

Si visita la dirección desde su navegador web deberá de poder visualizar el siguiente mensaje:

![Mensaje root de la aplicación](./img/mensaje_bienvenida.png)

Si ha logrado ver esa pantalla, entonces ha logrado hacer que la aplicación funcione correctamente.

## Docker

Si prefiere utilizar la aplicación dockerizada, puede ejecutar el siguiente comando desde el *root* del proyecto, en donde está localizado el **Dockerfile** (*Nota: recuerde utilizar el sudo si es que no lo deja correr comandos de docker*)

```sh
docker build -t "nombre_de_la_imagen" .
```

Una vez creada la imagen con éxito, puede crear un contenedor con el siguiente comando

```sh
docker run --rm -d --env-file ruta/del/archivo/.env -p 8000:80 --name nombre_del_contenedor nombre_de_la_imagen
```

### Explicación de las opciones y parámetros:

+ --rm: Este parámetro puede ser opcional, si desea que el contenedor que creó no sea borrado elimine esta opción
+ -d: Es recomendable correr el contenedor en segundo plano para dejarlo funcionando en un servidor, pero si quiere ver el output de la aplicación puede correrlo en primer plano dejando este comando
+ --env-file: Es obligatorio dejar esta opción y pasarle como parámetro un archivo con las variables de entorno (también es posible pasar las variables una por una con la opción -e, pero al ser una gran cantidad de variables es recomendable utilizar un fichero .env)
+ --name: Este parámetro puede ser opcional, puede elegir el nombre del contenedor
+ -p: Puede elegir el puerto que usted desee en lugar del puerto 8000 para desplegar la API

Si ha ejecutado correctamente los pasos, puede ingresar a http://localhost/ en el puerto que usted ha escogido para correr la aplicación.